﻿using System;

namespace exercise_14
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();

            //Declare variables
            var birthMonth = "";

            //Print to screen message asking user to enter birth month
            Console.WriteLine("Hello - Please enter the month in which you were born.");
            
            //Allow user to type in month of birth - store as var birthMonth
           birthMonth = Console.ReadLine();

           Console.ReadKey();
        }
    }
}
